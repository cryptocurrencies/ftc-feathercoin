# ---- Base Node ---- #
FROM ubuntu:16.04 AS base
RUN apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 8842ce5e && \
    echo "deb http://ppa.launchpad.net/bitcoin/bitcoin/ubuntu xenial main" > /etc/apt/sources.list.d/bitcoin.list
RUN apt-get update && \
    apt-get install -y build-essential libtool autotools-dev libboost-all-dev libdb4.8-dev libminiupnpc-dev libdb4.8++-dev automake pkg-config libssl-dev libevent-dev bsdmainutils git-core python3 && \
    apt-get upgrade -y && \
    apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

# ---- Build Source ---- #
FROM base as build
# Checkout latest Sourcecode
RUN git clone https://github.com/FeatherCoin/Feathercoin.git /opt/feathercoin
# Build All the Dependencies from the build folder
RUN cd /opt/feathercoin && \
    git checkout v0.16.0 && \
    ./autogen.sh && \
    ./configure --enable-cxx --disable-shared --with-pic --prefix=$BDB_PREFIX --disable-tests --without-gui --without-qrcode --with-miniupnpc --enable-upnp-default
RUN cd /opt/feathercoin && \
    make

# ---- Release ---- #
FROM ubuntu:16.04 as release
RUN apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 8842ce5e && \
    echo "deb http://ppa.launchpad.net/bitcoin/bitcoin/ubuntu xenial main" > /etc/apt/sources.list.d/bitcoin.list
RUN apt-get update && \
    apt-get install -y  libboost-all-dev libdb4.8-dev libdb4.8++-dev libminiupnpc-dev libevent-dev && \
    apt-get clean && rm -rf /var/lib/apt/lists/ /tmp/ /var/tmp/*
RUN groupadd -r feathercoin && useradd -r -m -g feathercoin feathercoin
RUN mkdir /data
RUN chown feathercoin:feathercoin /data
COPY --from=build /opt/feathercoin/src/feathercoind /usr/local/bin/feathercoind
COPY --from=build /opt/feathercoin/src/feathercoin-cli /usr/local/bin/feathercoin-cli
USER feathercoin
VOLUME /data
EXPOSE 9336 9337
CMD ["/usr/local/bin/feathercoind", "-datadir=/data", "-conf=/data/feathercoin.conf", "-server", "-txindex", "-printtoconsole" ]
